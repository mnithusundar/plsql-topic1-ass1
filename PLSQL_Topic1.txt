TOPIC 1: DECLARING VARIABLE

ASSIGNMENT 1
a) Option i. DECLARE iNumber NUMBER(4);  in all other three options datatype and variable name doesnot match and iNumber NUMBER(4) is the correct option with correct syntax.

b) NULL

c) Electronic CITY
   Talent Transformation CTE

ASSIGNMENT 2
a) Declare
	iFirst NUMBER(3);
	iSecond NUMBER(3);
	ires Number(3);
   Begin
	iFirst := &first;
	iSecond := &second;
	ires=(iFirst * iSecond/ iSecond);
	DBMS_OUTPUT.PUT_LINE(ires);
   End;
